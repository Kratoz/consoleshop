﻿namespace ConsoleShop.Constants
{
    public static class StringConstants
    {
        public const string UserName = "Enter username";
        public const string Password = "Enter password";
        public const string InvalidUser = "User not found";
        public const string InvalidLogIn = "Invalid username or password \npress any key to continue";
        public const string CatalogTableHead = "               name|    price|amount left";
        public const string CartTableHead = "               name|    price|amount";
        public const string ExistUserName = "Username already exist, enter another";
        public const string UserDataPathKey = "UserDataPath";
        public const string PurchaseHistoryPathKey = "PurchaseHistoryPath";
        public const string CatalogPathKey = "CatalogPath";
        public const string ShortUserName = "Username lenght should be > 4";
        public const string InvalidUserName = "Username must contain only letters or digits";
        public static readonly string[] LogInMenuOptions = {"Login", "Register"};
        public static readonly string[] ShopMenuOptions = {"Show catalog", "Show cart", "Show purchase history", "Search in catalog"};
        public const string LoginMenuNavigateHint = "Use arrows to navigate, enter to confirm, ESC to go back\\exit";
        public const string CatalogNavigateHint = "Use arrow to navigate, enter to show product description, ESC to go back";
        public const string ProductMenuNavigateHint = "Press enter to add product to cart or ESC to Go back";
        public const string InvalidProductAmount = "Invalid product amount, try again";
        public const string ProductAmount = "Enter product amount";
        public const string NoProductLeft = "No product left, come againt later";
        public const string InvalidProductAmountInCart = "Product amount in cart + product amount to add > product amount left";
        public const string NothingToSHow = "There is nothing to show";
        public const string ProductsBought = "Products bought: ";
        public const string KeyToContinue = "Press any key to continue";
        public const string CartNavigateHint = "Use arrows to navigate, " +
            "press del to delete product from cart, enter to checkout, ESC to go back";
        public const string SearchMessage = "Enter product name u want to find";
    }
}
