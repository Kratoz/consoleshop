﻿using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Storage;
using System;
using System.Collections.Generic;

namespace ConsoleShop.Validators
{
    public class Validator
    {
        private readonly DataStorage<Account> userStorage;
        private const int MinimalUsernameLength = 5;
        private const int MinimalMenuAllowedChoice = 0;

        public Validator()
        {
            userStorage = new DataStorage<Account>(StringConstants.UserDataPathKey);
        }

        public bool ValidateLogIn(string userName, string password)
        {
            List<Account> accounts = userStorage.GetDataFromStorage();

            if (accounts.Exists(account => account.Name.Equals(userName, StringComparison.CurrentCultureIgnoreCase) && account.Password == password))
            {
                return true;
            }

            Console.WriteLine(StringConstants.InvalidLogIn);
            Console.ReadKey(true);

            return false;
        }

        public bool ValidateNewUserName(string userName)
        {
            if (userName.Length < MinimalUsernameLength)
            {
                Console.WriteLine(StringConstants.ShortUserName);

                return false;
            }
            else if (userStorage.GetDataFromStorage().Exists(user => user.Name.Equals(userName, StringComparison.CurrentCultureIgnoreCase)))
            {
                Console.WriteLine(StringConstants.ExistUserName);

                return false;
            }
            else
            {
                for (var i = 0; i < userName.Length; i++)
                {
                    if (!char.IsLetterOrDigit(userName[i]))
                    {
                        Console.WriteLine(StringConstants.InvalidUserName);

                        return false;
                    }
                }
            }

            return true;
        }

        public int ValidateMenuOptionChoice(int menuSize, int choice)
        {
            if (choice > menuSize)
            {
                return --choice;
            }
            else if (choice < MinimalMenuAllowedChoice)
            {
                return ++choice;
            }

            return choice;
        }

        public bool ValidateProductAmount(Product product, int productAmount, List<Product> cart)
        {
            if (product.Amount < productAmount || productAmount < 1)
            {
                Console.WriteLine(StringConstants.InvalidProductAmount);

                return false;
            }
            else if (
                cart.Exists(cartProduct => cartProduct.Id == product.Id) &&
                cart[cart.FindIndex(cartProduct => cartProduct.Id == product.Id)].Amount + productAmount > product.Amount)
            {
                Console.WriteLine(StringConstants.InvalidProductAmountInCart);

                return false;
            }

            return true;
        }
    }
}
