﻿using Microsoft.Extensions.Configuration;

namespace ConsoleShop.Configuration
{
    public class AppConfiguration
    {
        private readonly IConfiguration Config;

        public AppConfiguration()
        {
            Config = new ConfigurationBuilder().AddJsonFile("Configuration\\appsettings.json").Build();
        }

        public string GetFilepathValue(string key)
        {
            return Config.GetValue<string>($"FilePaths:{key}");
        }

        public int GetTimerValue()
        {
            return Config.GetValue<int>("Timer:Time");
        }
    }
}
