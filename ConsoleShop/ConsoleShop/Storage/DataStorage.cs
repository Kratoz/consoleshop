﻿using ConsoleShop.Configuration;
using System.Collections.Generic;
using System.Text.Json;

namespace ConsoleShop.Storage
{
    public class DataStorage<T>
    {
        private readonly string filePath;

        public DataStorage(string dataPathKey)
        {
            AppConfiguration config = new AppConfiguration();
            filePath = config.GetFilepathValue(dataPathKey);
        }

        public List<T> GetDataFromStorage()
        {
            List<T> catalog = new List<T>();

            if (System.IO.File.Exists(filePath))
            {
                catalog = JsonSerializer.Deserialize<List<T>>(System.IO.File.ReadAllText(filePath));
            }

            return catalog;
        }

        public void AddDataToStorage(T data)
        {
            List<T> catalog = GetDataFromStorage();
            catalog.Add(data);
            System.IO.File.WriteAllText(filePath, JsonSerializer.Serialize(catalog));
        }

        public void OverwriteData(List<T> newData)
        {
            System.IO.File.WriteAllText(filePath, JsonSerializer.Serialize(newData));
        }
    }
}
