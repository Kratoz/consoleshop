﻿using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Services;
using System;
using System.Collections.Generic;

namespace ConsoleShop
{
    public class Menu
    {
        private readonly ConsoleService consoleService;
        private readonly ShopService shopService;

        public Menu()
        {
            consoleService = new ConsoleService();
            shopService = new ShopService();
        }

        public void CreateMenu()
        {
            while (ShowLoginMenu())
            {
                var loop = true;

                while (loop)
                {
                    loop = ShowShopMenu();
                }
            }
        }
        
        public bool ShowLoginMenu()
        {
            var result = false;
            var loop = false;

            while (!loop)
            {
                switch (consoleService.NavigateOnMenu(StringConstants.LogInMenuOptions))
                {
                    case 0:
                        loop = shopService.Login(consoleService.GetStringValue(StringConstants.UserName), consoleService.GetStringValue(StringConstants.Password));
                        result = loop;
                        break;
                    case 1:
                        shopService.RegisterNewUser(consoleService.GetNewUsername(), consoleService.GetStringValue(StringConstants.Password));
                        break;
                    case -1:
                        loop = true;
                        break;
                }
            }

            return result;
        }

        public bool ShowShopMenu()
        {
            var loop = true;

            while (loop)
            {
                switch (consoleService.NavigateOnMenu(StringConstants.ShopMenuOptions))
                {
                    case 0:
                        ShowCatalogMenu(shopService.GetCatalog());
                        break;
                    case 1:
                        ShowCartMenu();
                        break;
                    case 2:
                        consoleService.ShowOrderHistory(shopService.LoginedUsername);
                        consoleService.PressKey();
                        break;
                    case 3:
                        ShowCatalogMenu(shopService.SearchInCatalog(consoleService.GetStringValue(StringConstants.SearchMessage)));
                        break;
                    case -1:
                        return false;
                }
            }

            return true;
        }

        public void ShowCatalogMenu(List<Product> catalog)
        {
            var loop = true;

            while (loop)
            {
                var catalogChoice = consoleService.NavigateOnCatalog(catalog);

                switch (catalogChoice)
                {
                    case -1:
                        loop = false;
                        break;
                    default:
                        ShowProductMenu(catalogChoice);
                        break;
                }
            }
        }

        public void ShowProductMenu(int productId)
        {
            var loop = true;

            while (loop)
            {
                Console.Clear();
                consoleService.ShowProduct(productId, shopService.Cart);
                Console.WriteLine(StringConstants.ProductMenuNavigateHint);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Enter:
                        shopService.AddProductToCart(productId, consoleService.GetProductAmount(productId, shopService.Cart));
                        break;
                    case ConsoleKey.Escape:
                        loop = false;
                        break;
                    default:
                        break;
                }
            }
        }

        public void ShowCartMenu()
        {
            var loop = true;

            while (loop)
            {
                int cartChoice = consoleService.NavigateOnCart(shopService.Cart);
                
                switch (cartChoice)
                {
                    case -1:
                        loop = false;
                        break;
                    case -2:
                        shopService.CheckOut();
                        loop = false;
                        break;
                    default:
                        shopService.RemoveProductFromCart(cartChoice);
                        break;
                }
            }
        }
    }
}
