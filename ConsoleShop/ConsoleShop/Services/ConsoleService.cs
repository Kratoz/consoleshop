﻿using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Storage;
using ConsoleShop.Validators;
using System;
using System.Collections.Generic;

namespace ConsoleShop.Services
{
    public class ConsoleService 
    {
        private readonly DataStorage<Product> catalogStorage;
        private readonly DataStorage<Order> purchaseStorage;
        private readonly Validator validator;

        public ConsoleService()
        {
            catalogStorage = new DataStorage<Product>(StringConstants.CatalogPathKey);
            purchaseStorage = new DataStorage<Order>(StringConstants.PurchaseHistoryPathKey);
            validator = new Validator();
        }

        public string GetStringValue(string message)
        {
            Console.WriteLine(message);

            return Console.ReadLine();
        }

        public string GetNewUsername()
        {
            var loop = false;
            var userName = string.Empty;

            while (!loop)
            {
                Console.WriteLine(StringConstants.UserName);

                userName = Console.ReadLine();
                loop = validator.ValidateNewUserName(userName);

                if (!loop)
                {
                    PressKey();
                }
            }

            return userName;
        }

        public int NavigateOnMenu(string[] menuOptions)
        {
            var optionChoice = 0;

            while (true)
            {
                ShowMenuOptions(menuOptions, optionChoice);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                        optionChoice--;
                        optionChoice = validator.ValidateMenuOptionChoice(menuOptions.Length - 1, optionChoice);
                        break;
                    case ConsoleKey.DownArrow:
                        optionChoice++;
                        optionChoice = validator.ValidateMenuOptionChoice(menuOptions.Length - 1, optionChoice);
                        break;
                    case ConsoleKey.Enter:
                        return optionChoice;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }

        public void ShowMenuOptions(string[] options, int choice)
        {
            Console.Clear();

            for (var i = 0; i < options.Length; i++)
            {
                if (i == choice)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }

                Console.WriteLine(options[i]);
                Console.ResetColor();
            }

            Console.WriteLine(StringConstants.LoginMenuNavigateHint);
        }

        public int NavigateOnCatalog(List<Product> catalog)
        {
            var catalogChoice = 0;

            while (true)
            {
                ShowCatalog(catalog, catalogChoice);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                        catalogChoice--;
                        catalogChoice = validator.ValidateMenuOptionChoice(catalog.Count - 1, catalogChoice);
                        break;
                    case ConsoleKey.DownArrow:
                        catalogChoice++;
                        catalogChoice = validator.ValidateMenuOptionChoice(catalog.Count - 1, catalogChoice);
                        break;
                    case ConsoleKey.Enter:
                        return catalog[catalogChoice].Id;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }

        public void ShowCatalog(List<Product> catalog, int catalogChoice)
        {
            Console.Clear();
            Console.WriteLine(StringConstants.CatalogTableHead);

            for (var i = 0; i < catalog.Count; i++)
            {
                if (i == catalogChoice)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }

                Console.WriteLine($"{catalog[i].Name,19}|{catalog[i].Price,9}|{catalog[i].Amount}");
                Console.ResetColor();
            }

            Console.WriteLine(StringConstants.CatalogNavigateHint);
        }

        public void ShowProduct(int productId, List<Product> cart)
        {
            int amountInCart = 0;
            Product product = catalogStorage.GetDataFromStorage().Find(product => product.Id == productId);

            if (cart.Exists(item => item.Id == productId))
            {
                amountInCart = cart.Find(item => item.Id == productId).Amount;
            }

            Console.WriteLine($"{product.Name}\n{product.Description}\nPrice: {product.Price}\nAmount left:{product.Amount}(in cart:{amountInCart})");
        }

        public int GetProductAmount(int productId, List<Product> cart)
        {
            var loop = false;
            var productAmount = -1;
            Product product = catalogStorage.GetDataFromStorage().Find(product => product.Id == productId);

            if (product.Amount < 1 ||
                (cart.Exists(prod => prod.Id == productId) && product.Amount - cart.Find(prod => prod.Id == productId).Amount < 1))
            {
                Console.WriteLine(StringConstants.NoProductLeft);
                PressKey();

                return 0;
            }

            while (!loop)
            {
                Console.WriteLine(StringConstants.ProductAmount);
                int.TryParse(Console.ReadLine(), out productAmount);
                loop = validator.ValidateProductAmount(product, productAmount, cart);
            }

            return productAmount;
        }

        public int NavigateOnCart(List<Product> cart)
        {
            var choice = 0;

            while (true)
            {
                ShowCart(cart, choice);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                        choice--;
                        choice = validator.ValidateMenuOptionChoice(cart.Count - 1, choice);
                        break;
                    case ConsoleKey.DownArrow:
                        choice++;
                        choice = validator.ValidateMenuOptionChoice(cart.Count - 1, choice);
                        break;
                    case ConsoleKey.Delete:
                        return cart[choice].Id;
                    case ConsoleKey.Escape:
                        return -1;
                    case ConsoleKey.Enter:
                        return -2;
                }
            }
        }

        public void ShowCart(List<Product> cart, int cartProductChoice)
        {
            Console.Clear();

            decimal priceTotal = 0;

            cart.ForEach(product => priceTotal += product.Price * product.Amount);
            Console.WriteLine(StringConstants.CartTableHead);

            for (var i = 0; i < cart.Count; i++)
            {
                if (i == cartProductChoice)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }

                Console.WriteLine($"{cart[i].Name,19}|{cart[i].Price,9}|{cart[i].Amount}");
                Console.ResetColor();
            }

            Console.WriteLine($"Price total: {priceTotal}");
            Console.WriteLine(StringConstants.CartNavigateHint);
        }

        public void ShowOrderHistory(string customerName)
        {
            List<Order> orderHistory = purchaseStorage.GetDataFromStorage();
            orderHistory.RemoveAll(order => !order.CustomerName.Equals(customerName, System.StringComparison.CurrentCultureIgnoreCase));

            Console.Clear();

            if (orderHistory.Count > 0)
            {
                foreach (var item in orderHistory)
                {
                    Console.WriteLine(StringConstants.ProductsBought);

                    foreach (var products in item.ProductsBought)
                    {
                        Console.WriteLine($"{products.Name}({products.Amount}) price: {products.Price}");
                    }

                    Console.WriteLine($"Order date : {item.PurchaseTime}\nPrice total: {item.PriceTotal}");
                }
            }
            else
            {
                Console.WriteLine(StringConstants.NothingToSHow);
            }
        }

        public void PressKey()
        {
            Console.WriteLine(StringConstants.KeyToContinue);

            Console.ReadKey(true);
        }
    }
}
