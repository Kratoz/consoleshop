﻿using ConsoleShop.Configuration;
using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Storage;
using ConsoleShop.Validators;
using System;
using System.Collections.Generic;
using System.Timers;

namespace ConsoleShop.Services
{
    public class ShopService
    {
        private readonly DataStorage<Product> catalogStorage;
        private readonly DataStorage<Order> purchaseStorage;
        private readonly DataStorage<Account> userStorage;
        private readonly Validator validator;
        private Timer cartTimer;
        private readonly AppConfiguration config;

        public ShopService()
        {
            config = new AppConfiguration();
            Cart = new List<Product>();
            catalogStorage = new DataStorage<Product>(StringConstants.CatalogPathKey);
            purchaseStorage = new DataStorage<Order>(StringConstants.PurchaseHistoryPathKey);
            userStorage = new DataStorage<Account>(StringConstants.UserDataPathKey);
            validator = new Validator();
        }

        public List<Product> Cart { get; private set; }

        public string LoginedUsername { get; private set; }

        public bool Login(string username, string password)
        {
            if (validator.ValidateLogIn(username, password))
            {
                LoginedUsername = username;

                return true;
            }

            return false;
        }

        public void RegisterNewUser(string username, string password)
        {
            userStorage.AddDataToStorage(new Account(username, password));
        }

        public void AddProductToCart(int productId, int productAmount)
        {
            if (Cart.Count == 0)
            {
                cartTimer = new Timer(config.GetTimerValue());
                cartTimer.Elapsed += OnTimedEvent;
                cartTimer.Start();
            }

            if (Cart.Exists(product => product.Id == productId))
            {
                Cart[Cart.FindIndex(product => product.Id == productId)].Amount += productAmount;
            }
            else
            {
                Product product = (Product)catalogStorage.GetDataFromStorage().Find(product => product.Id == productId).Clone();
                product.Amount = productAmount;

                Cart.Add(product);
            }
        }

        public void RemoveProductFromCart(int productId)
        {
            Cart.RemoveAll(product => product.Id == productId);

            if (Cart.Count == 0)
            {
                cartTimer.Dispose();
            }
        }

        public void CheckOut()
        {
            Order order = new Order(Cart, LoginedUsername);
            List<Product> catalog = catalogStorage.GetDataFromStorage();

            foreach (var item in Cart)
            {
                catalog[catalog.FindIndex(product => product.Id == item.Id)].Amount -= item.Amount;
            }

            catalogStorage.OverwriteData(catalog);
            purchaseStorage.AddDataToStorage(order);
            Cart.Clear();
            cartTimer.Dispose();
        }

        public List<Product> SearchInCatalog(string itemToSearch)
        {
            List<Product> catalog = catalogStorage.GetDataFromStorage();
            List<Product> result = new List<Product>();

            result.AddRange(catalog.FindAll(item => item.Name.Contains(itemToSearch, StringComparison.CurrentCultureIgnoreCase)));

            return result;
        }
        
        public List<Product> GetCatalog()
        {
            return catalogStorage.GetDataFromStorage();
        }

        public void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Cart.Clear();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Cart was clear");
            Console.ResetColor();
            ((Timer)source).Dispose();
        }
    }
}
