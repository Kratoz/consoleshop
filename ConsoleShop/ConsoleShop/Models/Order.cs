﻿using System;
using System.Collections.Generic;

namespace ConsoleShop.Models
{
    public class Order
    {
        public Order(List<Product> cart, string customerName)
        {
            ProductsBought = cart;
            CustomerName = customerName;
            PurchaseTime = DateTime.Now;

            cart.ForEach(product => PriceTotal += product.Price * product.Amount);
        }

        public Order()
        {
        }

        public List<Product> ProductsBought { get; set; }

        public DateTime PurchaseTime { get; set; }

        public string CustomerName { get; set; }

        public decimal PriceTotal { get; set; }
    }
}
