﻿namespace ConsoleShop.Models
{
    public class Account
    {
        public Account(string name, string password)
        {
            Name = name;
            Password = password;
        }

        public Account()
        {

        }

        public string Name { get; set; }

        public string Password { get; set; }
    }
}
