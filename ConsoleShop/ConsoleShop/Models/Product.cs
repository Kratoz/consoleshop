﻿using System;

namespace ConsoleShop.Models
{
    public class Product : ICloneable
    {
        private static int LastId = 0;

        public Product(string name, decimal price, string description)
        {
            Name = name;
            Price = price;
            Description = description;
            Id = ++LastId;
        }

        public Product()
        {
        }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }

        public int Amount { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
